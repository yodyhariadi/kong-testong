start:
	docker run -d --name kong-ee-db-less --network=kong-ee-net \
      -e "KONG_DATABASE=off" \
      -e "KONG_PROXY_ACCESS_LOG=/dev/stdout" \
      -e "KONG_ADMIN_ACCESS_LOG=/dev/stdout" \
      -e "KONG_PROXY_ERROR_LOG=/dev/stderr" \
      -e "KONG_ADMIN_ERROR_LOG=/dev/stderr" \
      -e "KONG_ADMIN_LISTEN=0.0.0.0:8001" \
      -e "KONG_ADMIN_GUI_URL=http://localhost.kong.db-less:8002" \
        -p 8000:8000 \
        -p 8443:8443 \
        -p 8001:8001 \
        -p 8444:8444 \
        -p 8002:8002 \
        -p 8445:8445 \
        -p 8003:8003 \
        -p 8004:8004 \
        kong-ee

load-config:
	http :8001/config config=@awesomeProject.yml

stop:
	docker stop kong-ee-db-less
	docker rm kong-ee-db-less